from django.contrib import messages
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth import login as dj_login, logout as dj_logout
from django.views.decorators.csrf import csrf_exempt

from .forms import *
from .models import test_case


class login(View):
    def get(self, request):
        if request.user.is_authenticated:
            if request.user.is_superuser:
                return redirect('/admin-panel')
            return redirect('/user/main')
        next = request.GET.get('next', None)
        return render(request, 'login.html', {'next': next})

    def post(self, request):
        if request.user.is_authenticated:
            return redirect('/user/main')
        form = Login(request.POST)
        if form.is_valid():
            dj_login(request, form.user_cache)
            if request.GET.get('next', False):
                next = request.GET.get('next')
                return redirect(next)
            if request.user.is_superuser:
                return redirect('/admin-panel')
            return redirect('/')
        return render(request, 'login.html', {'form': form})


class index(View):
    def get(self, request):
        return render(request, 'index.html')


class logout(View):
    def get(self, request):
        dj_logout(request)
        return redirect('/accounts/login')

    def post(self, request):
        dj_logout(request)
        return redirect('/accounts/login')


class register(View):
    def get(self, request):
        return render(request, 'register.html')

    def post(self, request):
        form = Register(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            User.objects.create_user(username, email=email,
                                     password=password)
            messages.add_message(request, messages.SUCCESS, 'با موفقیت ثبت نام شدید لطفا وارد شوید.')
            return redirect('/accounts/login')
        else:
            return render(request, 'register.html', {'form': form})


@csrf_exempt
def test_case_view(request):
    data = request.POST.get('case')
    test_case.objects.create(case=data)
